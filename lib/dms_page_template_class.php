<?php

class DmsPageTemplate{
  public $old_file;
  public $new_file;
  public $raw_string;
  public $old_array;
  public $new_array;
  public $key;
  public $name;

  function __construct($json_file, $new_title){
    if(file_exists($json_file)){
      $this->old_file = $json_file;
      $this->raw_string = file_get_contents( $json_file );
      $first_line = strtok($this->raw_string, "\n");
      $this->old_array = json_decode($first_line, true);
      $this->new_array = $this->old_array;
      $this->get_key();
      $this->get_name();
      $this->remap_ids();
      $this->set_name($new_title);
      $this->set_key($new_title);
      $this->write();
    } else {
      throw new Exception("json file not found ($json_file)");
    }
  }
  
  function get_key(){
    $this->key = key($this->old_array['pl-user-templates']['draft']);
    return $this->key;
  }

  function get_name(){
    $this->name = $this->old_array['pl-user-templates']['draft'][$this->key]['name'];
    return $this->name;
  }

  function set_name($new_name){
    $this->name = $new_name;
    $this->new_array['pl-user-templates']['draft'][$this->key]['name'] = $this->name;
    return $this->name;
  }

  function set_key($key){
    $filtered_key = strtolower(preg_replace("/[^a-zA-Z0-9]+/", "", $key));
    $this->new_array['pl-user-templates']['draft'][$filtered_key] = $this->new_array['pl-user-templates']['draft'][$this->key];
    unset($this->new_array['pl-user-templates']['draft'][$this->key]);
    $this->key = $filtered_key;
    return $this->key;
  }

  function remap_ids(){
    $bands = $this->old_array['pl-user-templates']['draft'][$this->key]['map'];
    $old_section_id_list = array();
    foreach ($bands as $i => $band) {
      $band_id = $band['clone'];
      if( strlen($band_id) > 1 )
        $old_section_id_list[] = $band_id;
      if( isset($band['content']) ){
        foreach ($band['content'] as $section) {
          $section_id = $section['clone'];
          if( strlen($section_id) > 1 )
            $old_section_id_list[] = $section_id;
          if( isset($section['content']) ){
            foreach ($section['content'] as $subsection) {
              $subsection_id = $subsection['clone'];
              if( strlen($subsection_id) > 1 )
                $old_section_id_list[] = $subsection_id;
            }
          }
        }
      }
    }
    $json_string_raw = json_encode($this->new_array);

    // create an array of new IDs with the same number of entries
    for ($i=0; $i < count($old_section_id_list); $i++) { 
      $compare_string = substr($old_section_id_list[$i], 0, 1);
      if ($compare_string === 'u') {
        $new_section_id_list[$i] = 'u' . substr(uniqid(), -6);
      }
      else {
        $new_section_id_list[$i] = 's' . substr(uniqid(), -6);
      }
    }

    // // DEBUG OUTPUT 
    // $this->display_id_comparison($old_section_id_list, $new_section_id_list);

    $output_json = str_replace($old_section_id_list, $new_section_id_list, $json_string_raw);
    $this->new_array = json_decode($output_json, true);
  }

  function write(){
    $json_string_raw = json_encode($this->new_array);

    // new filename
    $this->new_file = str_replace(".json", " (".$this->name.").json", $this->old_file);

    $path_parts = pathinfo($this->new_file);
    
    $this->new_file_url = "http://".$_SERVER["SERVER_NAME"]."/dms-template-fixer/uploads/".$path_parts['basename'];

    // Create the new file or open it and wipe it
    $file_handle = fopen($this->new_file, "w");

    // Write to our opened file.
    if (fwrite($file_handle, $json_string_raw) === FALSE) {
      echo "Cannot write to file ($this->new_file)";
      exit;
    } 
  }

  function display_id_comparison($old, $new){
    echo "<div style='clear: both;'></div>";
    echo "<pre style='width: 240px; float: left; background: #eef'>Old IDs: " . print_r($old, true) . "</pre>";
    echo "<pre style='width: 240px; float: left; background: #edf'>New IDs: " . print_r($new, true) . "</pre>";
    echo "<div style='clear: both;'></div>";
  }
}