<?php

define('APP_ROOT', __DIR__);
define("UPLOAD_DIR", APP_ROOT."/uploads/");
if(!is_dir(UPLOAD_DIR)) {
  mkdir(UPLOAD_DIR);
} 
include APP_ROOT."/lib/dms_page_template_class.php";
include APP_ROOT."/lib/upload_error_handler.php";


if ($_POST['form_type'] == "upload"){
  
  if ($_FILES['dms_json_file']['error'] === UPLOAD_ERR_OK) {
    
    $tmp_file_name = $_FILES['dms_json_file']['tmp_name'];
    $file_name = $_FILES['dms_json_file']['name'];

    if ($tmp_file_name) {
      $json_extension = end((explode(".", $file_name)));
      if($json_extension === "json"){
        $content = file_get_contents($tmp_file_name);
        $first_line = strtok($content, "\n");
        $json = json_decode($first_line, true);
        if($json === null){
          echo "Error: <strong>{$_FILES['dms_json_file']['name']}</strong> is not a valid JSON file.<br>It's size is <strong>{$_FILES['dms_json_file']['size']}</strong> bytes";
        } else {
          // ensure a safe filename
          $name = preg_replace("/[^A-Z0-9._-]/i", "_", $file_name);
          // preserve file from temporary directory
          $success = move_uploaded_file($tmp_file_name, UPLOAD_DIR . $name);
          if (!$success) { 
            echo "<p>Unable to save file to ". UPLOAD_DIR . $name .".</p>";
            exit;
          } else {

            $dpt = new DmsPageTemplate(UPLOAD_DIR . $name, $_POST['template_name']);
            $file_url = $dpt->new_file_url;
            
            // Download the file
            header('Content-Type: application/octet-stream');
            header("Content-Transfer-Encoding: Binary"); 
            header("Content-disposition: attachment; filename=\"" . basename($file_url) . "\""); 
            readfile($file_url); 
            ignore_user_abort(true);
            unlink($dpt->new_file);
            unlink($dpt->old_file);


          }
        }
      } else {
        echo "Error: <strong>{$_FILES['dms_json_file']['name']}</strong> doesn't have a .json extension.<br>It's size is <strong>{$_FILES['dms_json_file']['size']}</strong> bytes";
      }
    } else {
      print "No file uploaded!";
    }
  } else { 
    throw new UploadException($_FILES['dms_json_file']['error']); 
  } 
}